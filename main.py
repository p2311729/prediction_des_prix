import pandas as pd
import modules.Info_Stats as Pack1
import modules.Public as Pack2
import modules.Formation as Pack3
import modules.Performance as Pack4
import modules.Test as Pack_Test
import unittest


test_suite = unittest.TestLoader()
test_suite = test_suite.loadTestsFromTestCase(Pack_Test.TestClasses)
test_runner = unittest.TextTestRunner()
test_result = test_runner.run(test_suite)
df = pd.read_csv("/builds/p2311729/"
                 "prediction_des_prix/Output/Lyon.csv")
Instance = Pack3.Formation(df, "Lyon")
Instance.formation_v1()
Instance.formation_v2()
Instance = Pack2.public(df, "Lyon")
Instance.Analyse_Attendance()
Instance = Pack1.info_stats(df, "Lyon")
Instance.Resultat_global()
Instance.Capitaine()
Instance.Resultat_detaille()
Instance = Pack4.performance(df, "Lyon")
Instance.Analyse_Performance()