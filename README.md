# Résultats et performances de l'Olympique Lyon 23/24

Ce dépôt contient le code permettant la visulalization des resultats et des performances de O.L suivant different indicateurs pour la saison 2023/2024.


## Source de données

Les données globales utilisées sont ici : https://fbref.com/en/comps/13/Ligue-1-Stats

Vous pouvez aussi télécharger la base de donnée utilisées [jeu de données](https://forge.univ-lyon1.fr/p2311729/prediction_des_prix/-/jobs/306372/artifacts/raw/Output/Lyon.csv?inline=false)


## Illustrations

Vous pouvez consulter le rapport sous forme d'un site web via ce lien [Rapport](http://p2311729.pages.univ-lyon1.fr/-/prediction_des_prix/-/jobs/306377/artifacts/Output/lien.html).

Vous pouvez aussi télécharger les output des codes via ce lien [Sortie](https://forge.univ-lyon1.fr/p2311729/prediction_des_prix/-/jobs/306373/artifacts/download).

## Membres

FARTASSI AHLAM p2312403

BOURRA HIBA p2311729

SGHIOURI ZAKARIAE p2312780

TAKNAOUI MOAD p1926579