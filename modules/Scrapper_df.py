import pandas as pd
from bs4 import BeautifulSoup
import requests
import os


class Scrapper():
    """
    Classe pour scrapper les données de football du site FBRef et
    exporter les données vers un fichier CSV.
    """
    def __init__(self, url, nom, repertoire):
        """
        Initialise la classe.
        Args:
            url(str): L'URL de base pour accéder au site.
            nom(str): nom pour le fichier exporter
            repertoire(str): chemin de repetroire d'exportation
        """
        self.nom = nom
        self.repertoire = repertoire
        self.url = url

    def preparation(self, df):
        """
        Supprimer matches qui sont pas encore jouer.
        Extraire juste les matches de Ligue1.
        Formater certainnes colonnes.
        Args:
            df(dataframe): df à traiter.
        Retourne:
            df: Une dataframe prète pour l'exportation
            après plusieurs transformation.
        """
        df = df.dropna(subset=['Result'])
        df = df[df['Comp'] == "Ligue 1"]
        df['Date'] = pd.to_datetime(df['Date'])
        df['Date'] = df['Date'].dt.strftime('%d-%m-%y')
        df['Captain'] = df['Captain'].str.split().str[1]
        df['GF'] = df['GF'].astype(int)
        df['GA'] = df['GA'].astype(int)
        return df

    def exporter(self, df):
        """
        Exporter la table df.
        Args:
            df(dataframe): df à exporter.
        """
        repertoire_final = os.path.join(self.repertoire, f'{self.nom}.csv')
        df.to_csv(repertoire_final, index=False)

    def scrapper(self):
        """
        Scrapper, Préparer, Exporter la table df contenant les resultats
        de Ligue 1 d'olympique lyon pour la saison 23/24.
        scrapper fait appel au deux fonction exporter et preparation.
        """
        data = requests.get(self.url)
        soup = BeautifulSoup(data.text)
        standings_table = soup.select('table.stats_table')[0]
        links = standings_table.find_all('a')
        links = [link.get("href") for link in links]
        links = [link for link in links if '/squads/' in link]
        links = [link for link in links if 'Lyon' in link]
        link_Lyon = links[0]
        Lyon_url = f"https://fbref.com{link_Lyon}"
        data = requests.get(Lyon_url)
        Tous_matches = pd.read_html(data.text, match="Scores & Fixtures")[0]
        self.exporter(self.preparation(Tous_matches))


url = "https://fbref.com/en/comps/13/Ligue-1-Stats"
nom = "Lyon"
u = "/builds/p2311729/prediction_des_prix/Output"
scraper_obj = Scrapper(url, nom, u)
df_Lyon = scraper_obj.scrapper()