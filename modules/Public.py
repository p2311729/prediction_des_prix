import numpy as np
import matplotlib.pyplot as plt


class public:
    """
     Classe permettant la visualiztaion de nombre de public
     present au stade Groupama et à l'exterieure pour chaque matches.
    """
    def __init__(self, df, nom):
        """
        Initialise l'instance avec les données des matches de O.L
        et le nom de l'équipe souhaiter afficher dans les graphes.
        Créer d'autre dataframe à partir de df.
        Args:
            df(dataframe): DataFrame contenant les resultats des matchs.
            nom(str): Nom de l'équipe qui va apparaitre sur les graphes.
        """
        self.nom = nom
        self.team_results = df
        self.away_df = self.team_results.query('Venue == "Away"')
        self.home_df = self.team_results.query('Venue == "Home"')

    def Analyse_Attendance(self):
        """
        Ploter le nombre d'audiance à domicile et à l'exterieure
        pour chaque matche de O.L, la saison 23/24.
        """
        fig, ax = plt.subplots(figsize=(8, 6))
        ax.plot(self.team_results["Date"],
                self.team_results["Attendance"], color="black")
        ax.scatter(self.away_df["Date"], self.away_df["Attendance"],
                   marker="o", color="red", label="Away")
        ax.scatter(self.home_df["Date"], self.home_df["Attendance"],
                   marker="o", color="blue", label="Home")
        ax.set_title(f"Nombre de public pour {self.nom}"
                     " durant la saison 23/24")
        ax.set_xlabel("Date")
        ax.set_ylabel("Nbr de public")
        ax.set_xticks(np.arange(len(self.team_results["Date"])))
        ax.set_xticklabels(self.team_results["Date"], rotation=45)
        ax.yaxis.grid(True, linestyle='--', linewidth=0.5)
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.grid(True, which='major', linestyle='-',
                linewidth=0.5, color='grey')
        handles = [plt.plot([], [], marker="o", ls="", color=color)[0]
                   for color in {'Home': 'blue', 'Away': 'red'}.values()]
        labels = list({'Home': 'blue', 'Away': 'red'}.keys())
        ax.legend(handles, labels, loc='upper left')
        plt.savefig("/builds/p2311729/prediction_des_prix/"
                    "Output/Nombre d'audiance a domicile"
                    f" et à l'éxterieure de {self.nom}.png")
        plt.show()