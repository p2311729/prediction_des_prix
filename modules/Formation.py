import numpy as np
import matplotlib.colors as mcolors
import matplotlib.pyplot as plt
import pandas as pd


class Formation:
    """
    Classe pour visualiser la répartition des résultats
    de matchs selon différentes formations pour l'O.L,
    à domicile et à l'extérieur.
    """
    def __init__(self, df, nom):
        """
        Initialise l'instance avec les données des matches de O.L
        et le nom de l'équipe souhaiter afficher dans les graphes.
        Créer d'autre dataframe à partir de df.
        Args:
            df(dataframe): DataFrame contenant les resultats des matchs.
            nom(str): Nom de l'équipe qui va apparaitre sur les graphes.
        """
        self.nom = nom
        couleurs_rgba = [
            mcolors.to_rgba(couleur) for couleur in
            ['blue', 'lightblue', 'lightcyan', 'lightgray',
             'salmon', 'rosybrown',  'lightgreen', 'lightyellow',
             'lime', 'orange', 'green', 'red']
        ]
        self.couleur_mapping = dict(zip(df['Formation'].unique(),
                                    couleurs_rgba))
        self.color_dict = self.couleur_mapping
        self.occurrences = df.groupby('Result')['Formation']
        self.occurrences = self.occurrences.value_counts()
        self.occurrences = self.occurrences.unstack(fill_value=0)
        self.df3 = df[df['Venue'] == 'Home']
        self.df2 = df[df['Venue'] == 'Away']
        self.dfs = [self.df3, self.df2]

    def formation_v1(self):
        """Visualise la répartition des résultats par formation
           pour l'équipe."""
        width = 0.6
        bottom = np.zeros(len(self.occurrences.index))
        fig, ax = plt.subplots()
        for col in self.occurrences.columns:
            values = self.occurrences[col]
            ax.bar(
                self.occurrences.index, values, width,
                bottom=bottom, label=col, color=self.color_dict[col]
            )
            bottom += values
        for bars in ax.containers:
            ax.bar_label(
                bars, label_type='center',
                labels=[f'{int(val)}'
                        if val != 0 else '' for val in bars.datavalues])
        ax.set_title("Répartition des résultats selon"
                     f" la formation de {self.nom}",
                     fontweight='bold', style='italic', fontsize=12,
                     color='red', pad=20)
        ax.set_ylabel('')
        ax.set_xticklabels(ax.get_xticklabels(), rotation=0)
        ax.legend(title='Root cause', fontsize='small')
        plt.tight_layout()
        file_path = ("/builds/p2311729/prediction_des_prix/"
                     f"Output/Formation({self.nom}).png")
        plt.savefig(file_path)
        plt.show()

    def formation_v2(self):
        """Visualise séparément les résultats selon formation
           à domicile et à l'extérieur."""
        width = 0.6
        titre = ['domicil', 'éxtérieur']
        fig, ax = plt.subplots(2, 1, figsize=(12, 12))
        for i, df in enumerate(self.dfs):
            occurrences = df.groupby('Result')['Formation']
            occurrences = occurrences.value_counts()
            occurrences = occurrences.unstack(fill_value=0)
            bottom = np.zeros(len(occurrences.index))
            for col in occurrences.columns:
                values = occurrences[col]
                ax[i].bar(
                    occurrences.index, values, width, bottom=bottom,
                    label=col, color=self.color_dict[col])
                bottom += values
            for bars in ax[i].containers:
                ax[i].bar_label(
                    bars, label_type='center',
                    labels=[f'{int(val)}'
                            if val != 0 else '' for val in bars.datavalues])
            title_text = ("Répartition des résultats selon"
                          f" la formation de {self.nom} à {titre[i]}")
            ax[i].set_title(title_text, fontweight='bold', style='italic',
                            fontsize=12, color='red', pad=20)
            ax[i].set_ylabel('')
            ax[i].set_xticklabels(ax[i].get_xticklabels(), rotation=0)
            ax[i].legend(title='Formation')
        plt.tight_layout()
        file_path = (f"/builds/p2311729/prediction_des_prix/"
                     f"Output/Formation(domicile_éxterieur)({self.nom}).png")
        plt.savefig(file_path)
        plt.show()