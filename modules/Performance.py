import matplotlib.pyplot as plt


class performance:
    """
    Classe permettant une analyse des performance de O.L
    En utilsant les but marquer, encaissé
    En utilsant aussi d'autre indicateure comme Xg Xga
    Xg(Expected Goals): représente la probabilité
    qu'un tir aboutisse à un but,
    basée sur des données historiques.
    Xga(Expected Goals Against): c'est l'opposé de Xg.
    Cela mesure la probabilité qu'une équipe encaisse
    un but sur la base des actions de l'équipe adverse.
    """
    def __init__(self, df, nom):
        """
        Initialise l'instance avec les données des matches de O.L
        et le nom de l'équipe souhaiter afficher dans les graphes.
        Créer d'autre dataframe à partir de df.
        Args:
            df(dataframe): DataFrame contenant les resultats des matchs.
            nom(str): Nom de l'équipe qui va apparaitre sur les graphes.
        """
        self.nom = nom
        Ga = df['GA'].cumsum()
        Xg = df['xG'].cumsum()
        Xga = df['xGA'].cumsum()
        self.tm = df.assign(but_cum=df['GF'].cumsum())
        self.tm = self.tm.assign(encaisse_cum=Ga)
        self.tm = self.tm.assign(estimmarque_cum=Xg)
        self.tm = self.tm.assign(estimencaisse_cum=Xga)

    def Analyse_Performance(self):
        """
        Fonction permettant de plotter la somme cummuler de :
        but marqué
        but encaissé
        Xg (Expected Goals)
        Xga (Expected Goals Against)
        """
        fig, ax = plt.subplots(figsize=(8, 6))
        x = self.tm['Date']
        y1 = self.tm['but_cum']
        y2 = self.tm['encaisse_cum']
        bars1 = ax.bar(x, y1, color='blue', label='Buts marqués')
        bars2 = ax.bar(x, y2, color='red', bottom=y1,
                       label='Buts encaissés')
        print(bars1, bars2)
        ax.set_title("Buts marqués et encaissés de"
                     f"{self.nom} durant la saison 23/24")
        ax.set_xlabel("Date")
        ax.set_ylabel("Nbr de but")
        ax.set_xticklabels(x, rotation=45, ha='right')
        ax.yaxis.grid(True, linestyle='--', linewidth=0.5)
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.grid(True, which='major', linestyle='-', linewidth=0.5,
                color='grey')
        ax.legend(loc="upper left")
        plt.savefig("/builds/p2311729/prediction_des_prix/"
                    "Output/But marqué_encaissé(V1).png")
        plt.show()
        fig, ax = plt.subplots(figsize=(8, 6))
        ax.plot(self.tm['Date'], self.tm['but_cum'],
                color='blue', label='Buts marqués', marker="o")
        ax.plot(self.tm['Date'],
                self.tm['encaisse_cum'],
                color='red', label='Buts encaissés', marker="o")
        ax.plot(self.tm['Date'],
                self.tm['estimmarque_cum'],
                color='green', label='Buts marqués(estimation)', marker="o")
        ax.plot(self.tm['Date'],
                self.tm['estimencaisse_cum'],
                color='orange', label='Buts encaissés(estimation)', marker="o")
        ax.set_title("Buts marqués et encaissés"
                     f"de {self.nom} durant la saison 23/24")
        ax.set_xlabel("Date")
        ax.set_ylabel("Nbr de but")
        ax.set_xticklabels(self.tm["Date"],
                           rotation=45, ha='right')
        ax.yaxis.grid(True, linestyle='--', linewidth=0.5)
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.grid(True, which='major', linestyle='-',
                linewidth=0.5, color='grey')
        ax.legend(loc="upper left")
        plt.savefig("/builds/p2311729/prediction_des_prix/"
                    "Output/But marqué_encaissé(V2).png")
        plt.show()