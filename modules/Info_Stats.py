import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


class info_stats:
    """
    Classe permettant de visulizer les states,
    resultats de OL durant cette saison.
    """
    def __init__(self, df, nom):
        """
        Initialise l'instance avec les données des matches de O.L
        et le nom de l'équipe souhaiter afficher dans les graphes.
        Créer d'autre dataframe à partir de df.
        Args:
            df(dataframe): DataFrame contenant les resultats des matchs.
            nom(str): Nom de l'équipe qui va apparaitre sur les graphes.
        """
        L = ['D', 'L', 'W']
        self.nom = nom
        self.team_df = df
        self.result_counts = self.team_df['Result'].value_counts()
        self.result_counts = self.result_counts.reindex(L)
        self.Captain = self.team_df['Captain']
        self.occurrences = dict(zip(*np.unique(self.Captain,
                                               return_counts=True)))
        self.cross_tab = pd.crosstab(df['Venue'], df['Result'])
        l1 = len(self.cross_tab.columns)
        l0 = self.cross_tab.index
        l2 = self.cross_tab.columns
        l3 = self.cross_tab.index
        l4 = self.cross_tab.values
        self.plot_data = pd.DataFrame({'Venue': l0.repeat(l1),
                                       'Result': list(l2) * len(l3),
                                       'Frequency': l4.flatten()})

    def Resultat_global(self):
        """
        Visulizer les resultats global de O.L,
        aussi selon domicile et exterieure.
        """
        plt.figure(figsize=(8, 6))
        plt.barh(self.result_counts.index, self.result_counts.values,
                 color=[{'W': 'blue', 'D': 'orange', 'L': 'red'}[result]
                        for result in self.result_counts.index])
        plt.title(f"Répartition des résultats de {self.nom}")
        plt.xlabel("Nombre de matchs")
        plt.ylabel("Résultat")
        plt.grid(True, which='major', linestyle='-',
                 linewidth=0.5, color='grey')
        plt.savefig(f"/builds/p2311729/prediction_des_prix/"
                    f"Output/Resultat_global({self.nom}).png")
        plt.show()
        plt.figure(figsize=(8, 6))
        sns.barplot(data=self.plot_data, x='Venue',
                    y='Frequency', hue='Result')
        plt.title(f'Resultat de {self.nom} à domicile et à éxterieur')
        plt.xlabel('Result')
        plt.ylabel('Fréquence')
        plt.legend(title='Résultats')
        plt.savefig("/builds/p2311729/prediction_des_prix/"
                    f"Output/Resultat_global_v2({self.nom}).png")
        plt.show()

    def Capitaine(self):
        """
        Présenter les capitaines de O.L pour la saion 23/24.
        """
        fig, ax = plt.subplots()
        bars = ax.barh(list(self.occurrences.keys()),
                       list(self.occurrences.values()), align='center')
        for bar in bars:
            width = bar.get_width()
            ax.text(width, bar.get_y() + bar.get_height() / 2,
                    f'{width}', va='center', ha='left', fontsize=10)
        ax.set_xlabel('Nombre de match')
        ax.set_ylabel('Capitains')
        ax.set_title(f'Capitains de {self.nom}')
        plt.savefig("/builds/p2311729/prediction_des_prix/"
                    f"Output/Capitain({self.nom}).png")
        plt.show()

    def Resultat_detaille(self):
        """
        Visulizer les resultats détailler de toute la saison match par match.
        """
        fig, ax = plt.subplots(figsize=(8, 6))
        ax.scatter(self.team_df['Date'], self.team_df['Result'],
                   color=[{'W': 'blue', 'D': 'orange', 'L': 'red'}[res]
                   for res in self.team_df['Result']])
        ax.set_title(f"Résultats détaillés de {self.nom}")
        ax.set_xlabel("Date")
        ax.set_ylabel("Résultat")
        ax.grid(True, which='major', linestyle='-', linewidth=0.5,
                color='grey')
        ax.set_xticklabels(self.team_df['Date'], rotation=45, ha='right')
        ax.set_ylim(-1.11, 3.21)
        for label, x, y in zip(self.team_df['Opponent'], self.team_df['Date'],
                               self.team_df['Result']):
            ax.annotate(label, xy=(x, y), xytext=(-0.071, 0.071),
                        textcoords='offset points', rotation=45)
        handles = [plt.plot([], [], marker="o", ls="", color=color)[0]
                   for color in
                   {'W': 'blue', 'D': 'orange', 'L': 'red'}.values()]
        labels = list({'W': 'blue', 'D': 'orange', 'L': 'red'}.keys())
        ax.legend(handles, labels, loc='upper left')
        plt.savefig("/builds/p2311729/prediction_des_prix/"
                    f"Output/Résultat détaillé par match({self.nom}).png")
        plt.show()