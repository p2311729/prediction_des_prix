import unittest
import pandas as pd
import modules.Info_Stats as Pack1
import modules.Public as Pack2
import modules.Formation as Pack3
import modules.Performance as Pack4


class TestClasses(unittest.TestCase):
    """Classe de tests pour vérifier les différentes fonctionnalités."""

    def setUp(self):
        """Initialise l'echantillion pour les tests."""
        df = pd.read_csv("/builds/p2311729/"
                         "prediction_des_prix/Output/Lyon.csv")
        self.df = df.head()[0:3]

    def test_performance(self):
        """Tester la classe performance."""
        perf = Pack4.performance(self.df, 'Lyon')
        self.assertEqual(perf.tm['but_cum'].tolist(), [1, 2, 2])
        self.assertEqual(perf.tm['encaisse_cum'].tolist(), [2, 6, 6])
        self.assertEqual(perf.tm['estimmarque_cum'][1], 2.1)
        self.assertEqual(perf.tm['estimencaisse_cum'][2], 6.1)

    def test_public(self):
        """Tester la classe publique."""
        pub = Pack2.public(self.df, 'Lyon')
        self.assertEqual(len(pub.home_df), 1)
        self.assertEqual(len(pub.away_df), 2)

    def test_formation(self):
        """Tester la classe formation."""
        form = Pack3.Formation(self.df, 'Lyon')
        expected_occurrences = {'4-2-3-1': {'D': 1, 'L': 2}}
        self.assertDictEqual(form.occurrences.to_dict(),
                             expected_occurrences)

    def test_info_stats(self):
        """Tester la classe info_stats."""
        stats = Pack1.info_stats(self.df, 'Lyon')
        expected_occurrences = {'Lacazette': 2,
                                'Tolisso': 1}
        self.assertDictEqual(stats.occurrences,
                             expected_occurrences)